# 2-10-19 simple threads in python

import threading

def worker(thread_num, A, maxvals, start_index, end_index):
    print('task ' + str(thread_num) + ' is starting, start = ' +
          str(start_index) + ' end = ' + str(end_index))
    max = -999999.0
    for i in range(start_index, end_index+1):
      if A[i] > max:
        max = A[i]
    print('task ' + str(thread_num) + ': max is ' + str(max))
    maxvals[thread_num] = max

#--------------------------------------------------------

def findmax(A):
    thread_list = []
    n = len(A)

    numtasks = 4
    maxvals = [0] * numtasks
    chunk = n // numtasks
    leftover = n % numtasks
    for i in range(numtasks-1):
      start = i*chunk
      end = start + chunk - 1
      t = threading.Thread(target=worker,
              args=(i, A, maxvals, start, end))
      t.start()
      thread_list.append(t)

    start = (numtasks-1)*chunk
    end = n-1
    t = threading.Thread(target=worker,
            args=(numtasks-1, A, maxvals, start, end))
    t.start()
    thread_list.append(t)

    for t in thread_list:
      t.join()

    m = maxvals[0]
    for i in range(numtasks):
      print('max val from task ' + str(i) + ' is ' + str(maxvals[i]))
      if maxvals[i] > m:
        m = maxvals[i]

    return(m)

#--------------------------------------------------------

def main():
  n = 110
  A = []
  for i in range(n):
    A.append(i)

  the_max = findmax(A)
  print('max of all values in A is ' + str(the_max)) 

main()
