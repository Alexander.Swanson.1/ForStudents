#include <pthread.h>
#include <stdio.h>

void *runner(void *param);

typedef struct {
  int val;
  int sum;
} SumStruct;

int main(int argc, char *argv[]) {
  SumStruct data;      /* holds data we share with child thread */
  pthread_t tid;       /* thread identifier */
  pthread_attr_t attr; /* thread attributes */

  if (argc != 2) {
    fprintf(stderr, "need an int value\n");
    return(8);
  }
  if (atoi(argv[1]) < 0) {
    fprintf(stderr, "val must be >= 0\n");
    return(8);
  }

  data.val = atoi(argv[1]);

  /* get default thread attributes */
  pthread_attr_init(&attr);

  /* create the thread */
  pthread_create(&tid, &attr, runner, &data);

  /* wait for the thread to terminate */
  pthread_join(tid, NULL);

  printf("sum = %d\n", data.sum);
  return(0);
}

void *runner(void *param) {
  SumStruct *data;
  int i, sum;

  sum = 0;
  data = (SumStruct *) param;

  printf("(R) I am runner; will sum integers to %d\n", data->val);

  for (i=0; i<=data->val; ++i)
    sum = sum + i;
  data->sum = sum;

  printf("(R) sum is %d\n", data->sum);

  pthread_exit(0);
}

