typedef struct StudentListNodeStruct {
  int id;
  char name[32];
  struct StudentListNodeStruct *next;
} StudentListNode;

int insertStudent(StudentListNode **list, int id, char *name);
int findStudent(StudentListNode *list, int id, char *name);
int deleteStudent(StudentListNode **list, int id);
int printList(StudentListNode *list);

