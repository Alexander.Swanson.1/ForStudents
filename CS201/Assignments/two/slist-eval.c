#include <stdio.h>
#include <string.h>
#include "slist.netid.h"

int checkResultInsert(int testNum, StudentListNode *theList, int rc,
                int rcExpected, StudentListNode *arr,
                int numEntriesExpected) {
  int i;
  int rtnval;
  StudentListNode *listNode;

  rtnval = 0;
  if (rc != rcExpected) {
    printf("*** test %d: mismatch in return code (%d vs. %d)\n",
           testNum, rc, rcExpected);
    rtnval = 1;
  }

  listNode = theList;
  i = 0;
  while (listNode != NULL && i < numEntriesExpected) {
    if (arr[i].id != listNode->id) {
      printf("*** test %d: mismatch in id in node #%d (%d vs. %d)\n",
             testNum, i, arr[i].id, listNode->id);
      rtnval = 1;
    }
    if ( strcmp(arr[i].name, listNode->name) ) {
      printf("*** test %d: mismatch in name in node #%d (%s vs. %s)\n",
             testNum, i, arr[i].name, listNode->name);
      rtnval = 1;
    }
    i = i + 1;
    listNode = listNode->next;
  }

  if (i != numEntriesExpected) {
    printf("*** test %d: not enough nodes in list (%d; expected %d)\n",
           testNum, i, numEntriesExpected);
    rtnval = 1;
  }

  if (listNode != NULL) {
    printf("*** test %d: too many nodes in list (expected %d)\n",
           testNum, numEntriesExpected);
    rtnval = 1;
  }

  return(rtnval);
}

//------------------------------------------------------------------

int checkResultFind(int testNum, int rc, int rcExpected,
                    char *name, char *nameExpected) {
  int rtnval;

  rtnval = 0;
  if (rc != rcExpected) {
    printf("*** test %d: mismatch in return code (%d vs. %d)\n",
           testNum, rc, rcExpected);
    rtnval = 1;
  }

  if (rcExpected == 0 && rc == 0) {
    if ( strcmp(name, nameExpected) ) {
      printf("*** test %d: mismatch in name (%s vs. %s)\n", testNum,
             name, nameExpected);
      rtnval = 1;
    }
  }

  return(rtnval);
}

//-------------------------------------------------

main() {
  StudentListNode *theList = NULL;
  char name[32];
  char buffer[32];
  int number;
  int rc, rtnval;

  rtnval = 0;
 
  strcpy(name, "Trey Burke"); 
  rc = insertStudent(&theList, 23, name);
  StudentListNode arr1[] = { {23, "Trey Burke"} };
  rtnval = checkResultInsert(1, theList, rc, 0, arr1, 1);

  strcpy(name, "Tim Hardaway Jr."); 
  rc = insertStudent(&theList, 3, name);
  StudentListNode arr2[] = { {3, "Tim Hardaway Jr."}, {23, "Trey Burke"} };
  rtnval += checkResultInsert(2, theList, rc, 0, arr2, 2);

  strcpy(name, "Emmanuel Mudiay"); 
  rc = insertStudent(&theList, 1, name);
  StudentListNode arr3[] = { {1, "Emmanuel Mudiay"}, {3, "Tim Hardaway Jr."},
      {23, "Trey Burke"} };
  rtnval += checkResultInsert(3, theList, rc, 0, arr3, 3);

  strcpy(name, "Lance Thomas"); 
  rc = insertStudent(&theList, 42, name);
  StudentListNode arr4[] = { {1, "Emmanuel Mudiay"}, {3, "Tim Hardaway Jr."},
      {23, "Trey Burke"}, {42, "Lance Thomas"} };
  rtnval += checkResultInsert(4, theList, rc, 0, arr4, 4);

  strcpy(name, "Alonzo Trier"); 
  rc = insertStudent(&theList, 14, name);
  StudentListNode arr5[] = { {1, "Emmanuel Mudiay"}, {3, "Tim Hardaway Jr."},
      {14, "Alonzo Trier"}, {23, "Trey Burke"}, {42, "Lance Thomas"} };
  rtnval += checkResultInsert(5, theList, rc, 0, arr5, 5);

  strcpy(name, "Sterling Brown"); 
  rc = insertStudent(&theList, 23, name);
  rtnval += checkResultInsert(6, theList, rc, 1, arr5, 5);

  //---------------------------------------------------------------

  number = 1;
  rc = findStudent(theList, number, name);
  rtnval += checkResultFind(7, rc, 0, name, "Emmanuel Mudiay");

  number = 2;
  rc = findStudent(theList, number, name);
  rtnval += checkResultFind(8, rc, 1, name, "");

  number = 42;
  rc = findStudent(theList, number, name);
  rtnval += checkResultFind(9, rc, 0, name, "Lance Thomas");

  number = 100;
  rc = findStudent(theList, number, name);
  rtnval += checkResultFind(10, rc, 1, name, "");

  number = 23;
  rc = findStudent(theList, number, name);
  rtnval += checkResultFind(11, rc, 0, name, "Trey Burke");

  //---------------------------------------------------------------

  number = 14;
  rc = deleteStudent(&theList, number);
  StudentListNode arr12[] = { {1, "Emmanuel Mudiay"}, {3, "Tim Hardaway Jr."},
      {23, "Trey Burke"}, {42, "Lance Thomas"} };
  rtnval += checkResultInsert(12, theList, rc, 0, arr12, 4);

  number = 1;
  rc = deleteStudent(&theList, number);
  StudentListNode arr13[] = { {3, "Tim Hardaway Jr."},
      {23, "Trey Burke"}, {42, "Lance Thomas"} };
  rtnval += checkResultInsert(13, theList, rc, 0, arr13, 3);

  number = 3;
  rc = findStudent(theList, number, name);
  rtnval += checkResultFind(14, rc, 0, name, "Tim Hardaway Jr.");

  number = 6;
  rc = deleteStudent(&theList, number);
  StudentListNode arr15[] = { {3, "Tim Hardaway Jr."},
      {23, "Trey Burke"}, {42, "Lance Thomas"} };
  rtnval += checkResultInsert(15, theList, rc, 1, arr15, 3);

  number = 3;
  rc = deleteStudent(&theList, number);
  StudentListNode arr16[] = { {23, "Trey Burke"}, {42, "Lance Thomas"} };
  rtnval += checkResultInsert(16, theList, rc, 0, arr16, 2);

  number = 42;
  rc = deleteStudent(&theList, number);
  StudentListNode arr17[] = { {23, "Trey Burke"} };
  rtnval += checkResultInsert(17, theList, rc, 0, arr17, 1);

  number = 1;
  rc = deleteStudent(&theList, number);
  StudentListNode arr18[] = { {23, "Trey Burke"} };
  rtnval += checkResultInsert(18, theList, rc, 1, arr17, 1);

  number = 23;
  rc = deleteStudent(&theList, number);
  StudentListNode arr19[] = { };
  rtnval += checkResultInsert(19, theList, rc, 0, arr19, 0);

  number = 1;
  rc = findStudent(theList, number, name);
  rtnval += checkResultFind(20, rc, 1, name, "");

  if (rtnval == 0) {
    printf("*** # failing tests = 0 (all 20 tests pass)\n");
    return(0);
  } else {
    printf("*** # failing tests = %d\n", rtnval);
    return(rtnval);
  }
}
