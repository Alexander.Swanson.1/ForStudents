#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pqueue.netid.h"

typedef struct {
  int priority;
  int id;
  char name[32];
} PQueueNodeProxy;

StudentRecord *createStudent(char *name, int id) {
  StudentRecord *studentRec;
  studentRec = (StudentRecord *) malloc(sizeof(StudentRecord));
  studentRec->id = id;
  strcpy(studentRec->name, name);
  return studentRec;
}

//---------------------------------------------------------------------------

int checkPQueue(int testNum, PQueueNode *pqueue,
            PQueueNodeProxy *pqueueExpected, int numEntriesExpected) {
  PQueueNode *pqn;
  StudentRecord *rec;
  int i, rtnval;

  rtnval = 0;
  i = 0;
  pqn = pqueue;
  while (pqn != NULL && i < numEntriesExpected) {
    if (pqn->priority != pqueueExpected[i].priority) {
      printf("*** test %d: mismatch in priority in node %d (got %d; expected %d)\n",
             testNum, i, pqn->priority, pqueueExpected[i].priority);
      rtnval = 1;
    }

    rec = (StudentRecord *) pqn->data;

    if (rec->id != pqueueExpected[i].id) {
      printf("*** test %d: mismatch in id in node %d (got %d; expected %d)\n",
             testNum, i, rec->id, pqueueExpected[i].id);
      rtnval = 1;
    }

    if (strcmp(rec->name, pqueueExpected[i].name)) {
      printf("*** test %d: mismatch in name in node %d (got %s; expected %s)\n",
             testNum, i, rec->name, pqueueExpected[i].name);
      rtnval = 1;
    }

    i = i + 1;
    pqn = pqn->next;
  }

  if (i != numEntriesExpected) {
    printf("*** test %d: not enough nodes in queue (got %d; expected %d)\n",
           testNum, i, numEntriesExpected);
    rtnval = 1;
  }

  if (pqn != NULL) {
    printf("*** test %d: too many nodes in list (expected %d)\n",
           testNum, numEntriesExpected);
    rtnval = 1;
  }

  return(rtnval);
}

//---------------------------------------------------------------------------

int checkRecord(int testNum, StudentRecord *rec,
                int idExpected, char *nameExpected) {
  int rtnval = 0;

  if (rec->id != idExpected) {
    printf("*** test %d: mismatch in id (got %d; expected %d)\n",
           testNum, rec->id, idExpected);
    rtnval = 1;
  }

  if (strcmp(rec->name, nameExpected)) {
    printf("*** test %d: mismatch in name in (got %s; expected %s)\n",
           testNum, rec->name, nameExpected);
    rtnval = 1;
  }

  return(rtnval);
}

//---------------------------------------------------------------------------

int pqueueChecker() {
  PQueueNode *pqueue = NULL;
  int rtnval = 0;
  StudentRecord *studentRec;

  studentRec = createStudent("Chris Paul", 1245);
  enqueue(&pqueue, 5, studentRec);

  PQueueNodeProxy pqueueExp[] = { {5, 1245, "Chris Paul"} };
  rtnval += checkPQueue(1, pqueue, pqueueExp, 1);

  studentRec = createStudent("James Harden", 2324);
  enqueue(&pqueue, 3, studentRec);

  PQueueNodeProxy pqueueExp2[] = { {3, 2324, "James Harden"}, {5, 1245, "Chris Paul"} };
  rtnval += checkPQueue(2, pqueue, pqueueExp2, 2);

  studentRec = createStudent("Kevin Love", 3794);
  enqueue(&pqueue, 8, studentRec);

  PQueueNodeProxy pqueueExp3[] = { {3, 2324, "James Harden"}, {5, 1245, "Chris Paul"}, {8, 3794, "Kevin Love"} };
  rtnval += checkPQueue(3, pqueue, pqueueExp3, 3);

  studentRec = createStudent("Dwyane Wade", 8345);
  enqueue(&pqueue, 7, studentRec);

  PQueueNodeProxy pqueueExp4[] = { {3, 2324, "James Harden"}, {5, 1245, "Chris Paul"}, {7, 8345, "Dwyane Wade"}, {8, 3794, "Kevin Love"} };
  rtnval += checkPQueue(4, pqueue, pqueueExp4, 4);

  studentRec = createStudent("Luol Deng", 3245);
  enqueue(&pqueue, 1, studentRec);

  PQueueNodeProxy pqueueExp5[] = { {1, 3245, "Luol Deng"}, {3, 2324, "James Harden"}, {5, 1245, "Chris Paul"}, {7, 8345, "Dwyane Wade"}, {8, 3794, "Kevin Love"} };
  rtnval += checkPQueue(5, pqueue, pqueueExp5, 5);

  studentRec = createStudent("Al Horford", 4326);
  enqueue(&pqueue, 1, studentRec);

  PQueueNodeProxy pqueueExp6[] = { {1, 3245, "Luol Deng"}, {1, 4326, "Al Horford"}, {3, 2324, "James Harden"}, {5, 1245, "Chris Paul"}, {7, 8345, "Dwyane Wade"}, {8, 3794, "Kevin Love"} };
  rtnval += checkPQueue(6, pqueue, pqueueExp6, 6);

  studentRec = createStudent("Jayson Tatum", 1287);
  enqueue(&pqueue, 8, studentRec);

  PQueueNodeProxy pqueueExp7[] = { {1, 3245, "Luol Deng"}, {1, 4326, "Al Horford"}, {3, 2324, "James Harden"}, {5, 1245, "Chris Paul"}, {7, 8345, "Dwyane Wade"}, {8, 3794, "Kevin Love"}, {8, 1287, "Jayson Tatum"} };
  rtnval += checkPQueue(7, pqueue, pqueueExp7, 7);

  /*---------------------------------------------------------------*/

  studentRec = peek(pqueue);
  rtnval += checkRecord(8, studentRec, 3245, "Luol Deng");

  studentRec = dequeue(&pqueue);
  rtnval += checkRecord(9, studentRec, 3245, "Luol Deng");

  studentRec = peek(pqueue);
  rtnval += checkRecord(10, studentRec, 4326, "Al Horford");

  studentRec = dequeue(&pqueue);
  rtnval += checkRecord(11, studentRec, 4326, "Al Horford");

  studentRec = peek(pqueue);
  rtnval += checkRecord(12, studentRec, 2324, "James Harden");

  /*---------------------------------------------------------------*/

  studentRec = createStudent("Donovan Mitchell", 934);
  enqueue(&pqueue, 2, studentRec);

  PQueueNodeProxy pqueueExp13[] = { {2, 934, "Donovan Mitchell"}, {3, 2324, "James Harden"}, {5, 1245, "Chris Paul"}, {7, 8345, "Dwyane Wade"}, {8, 3794, "Kevin Love"}, {8, 1287, "Jayson Tatum"} };
  rtnval += checkPQueue(13, pqueue, pqueueExp13, 6);

  studentRec = createStudent("Kyle Korver", 7328);
  enqueue(&pqueue, 3, studentRec);

  PQueueNodeProxy pqueueExp14[] = { {2, 934, "Donovan Mitchell"}, {3, 2324, "James Harden"}, {3, 7328, "Kyle Korver"}, {5, 1245, "Chris Paul"}, {7, 8345, "Dwyane Wade"}, {8, 3794, "Kevin Love"}, {8, 1287, "Jayson Tatum"} };
  rtnval += checkPQueue(14, pqueue, pqueueExp14, 7);

  /*---------------------------------------------------------------------*/

  int icount = 0;
  int rc, minPriority;

  while ( queueLength(pqueue) > 0 ) {
    int errorInIteration = 0;
    minPriority = getMinPriority(pqueue);
    if (minPriority != pqueueExp14[icount].priority) {
      printf("test %d: mismatch in priority: got %d expected %d\n",
             icount + 15, minPriority, pqueueExp14[icount].priority);
      errorInIteration = 1;
    }

    studentRec = dequeue(&pqueue);
    rc = checkRecord(icount + 15, studentRec, pqueueExp14[icount].id, pqueueExp14[icount].name);
    if (rc != 0)
      errorInIteration = 1;

    if (errorInIteration)
      ++rtnval;
    ++icount;
  }

  return(rtnval);
}

int main(int argc, char *argv[]) {
  int rc;

  rc = pqueueChecker();

  if (rc == 0) {
    printf("*** all tests passed\n");
    return(0);
  } else {
    printf("%d test(s) failed\n", rc);
    return(8);
  }

}
