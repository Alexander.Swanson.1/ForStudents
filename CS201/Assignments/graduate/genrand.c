#include <math.h>
#include <stdlib.h>

// generate a random value >= 0.0 having an exponential
// distribution, with mean = beta

double genrandexp(double beta) {
  double u, x;

  u = drand48();
  x = -beta * log(1.0 - u);
  return(x);
}

// to genarate a uniformly-distributed random value in
// in the range 0 <= r <= 1.0, use drand48()

// to set the seed value for the RNG, do this:
//
// long seed;
// time(&seed);
// srand48(seed);
