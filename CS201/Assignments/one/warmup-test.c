#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "warmup.h"

void handler(int signum) {
  printf("ERROR: got a signal %d\n", signum);
  exit(8);
}

int test1() {
  int n, sum, rc;
  int rtnval = 0;

  sum = 35;

  n = 15;
  rc = calculateSum(n, &sum);
  if (rc != 0) {
    printf("calculateSum failed I: expected zero return value\n");
    ++rtnval;
  }

  if (sum != 120) {
    printf("calculateSum failed II: expected sum of 120\n");
    ++rtnval;
  }

  n = -5;
  rc = calculateSum(n, &sum);
  if (rc != 1) {
    printf("calculateSum failed III: expected nonzero return value\n");
    ++rtnval;
  }

  return(rtnval);
}

int test2() {
  StudentData *sd;
  int rc;
  int rtnval = 0;

  char *name1 = "Matthew Stafford";
  int id = 8;

  rc = createRecord(id, name1, &sd); 
  if (rc != 0) {
    printf("createRecord failed I: expected zero return value\n");
    ++rtnval;
  }

  if (sd->id != id) {
    printf("createRecord failed II: expected id value of %d\n", id);
    ++rtnval;
  }

  if (strcmp(sd->name, name1) != 0) {
    printf("createRecord failed III: expected name value of %s\n", name1);
    ++rtnval;
  }

  char *name2 = "A Very Very Very Very Very Very Long Name";
  rc = createRecord(id, name2, &sd); 
  if (rc == 0) {
    printf("createRecord failed IV: expected nonzero return value\n");
    ++rtnval;
  }

  if (sd != NULL) {
    printf("createRecord failed V: expected NULL value for second parm\n");
    ++rtnval;
  }

  return(rtnval);
}

int main(int argc, char *argv[]) {
  int rtnval1, rtnval2;
  struct sigaction action;
  action.sa_handler = handler;
  sigaction(SIGSEGV, &action, NULL);

  rtnval1 = test1();
  rtnval2 = test2();
  if (rtnval1 != 0 || rtnval2 != 0) {
    printf("number of failing tests = %d\n", rtnval1 + rtnval2);
    return(8);
  } else {
    return(0);
  }
}
