typedef struct {
  char name[32];
  int id;
} StudentData;

int createRecord(int id, char *name, StudentData **record);

int calculateSum(int n, int *sum);

