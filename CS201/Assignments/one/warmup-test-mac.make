#NETID = jhibbele

OBJECTS = $(CODEOBJECTS) warmup.h

CODEOBJECTS = warmup.$(NETID).o warmup-test.o

warmup-test: $(OBJECTS)
	gcc $(CODEOBJECTS) -o warmup-test

warmup.h: warmup.$(NETID).h
	 cp warmup.$(NETID).h warmup.h

warmup.$(NETID).o: warmup.h warmup.$(NETID).c
	gcc -c -g -std=gnu99 warmup.$(NETID).c

warmup-test.o: warmup-test.c warmup.$(NETID).o warmup.h
	gcc -c -g warmup-test.c

clean:
	rm -f warmup-test.o warmup.$(NETID).o warmup-test
