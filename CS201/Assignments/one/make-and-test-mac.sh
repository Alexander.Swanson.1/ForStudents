if [[ "${#}" -ne 1 ]]
then
  echo "error -- command should be '$0 <netid>'"
  exit 8
fi

export NETID=$1

gnumake -f warmup-test-mac.make clean

gnumake -f warmup-test-mac.make
sts=$?
if [[ ${sts} -ne 0 ]]
then
  echo "error -- the make failed"
  exit 8
fi

if [[ ! -x warmup-test ]]
then
  echo "error -- can't find program warmup-test"
  exit 8
fi

echo "./warmup-test"
./warmup-test
