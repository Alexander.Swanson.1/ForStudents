#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "warmup.jhibbele.h"

int calculateSum(int n, int *sum) {
  int s;
  if (n < 0)
    return(1);

  s = 0;
  for (int i=1; i<=n; ++i)
    s = s + i;

  *sum = s;
  return(0);
}

int createRecord(int id, char *name, StudentData **record) {
  StudentData *sd;
  if (strlen(name) > 31) {
    *record = NULL;
    return(1);
  }

  sd = (StudentData *) malloc(sizeof(StudentData));
  sd->id = id;
  strcpy(sd->name, name);
  *record = sd;
  return(0);
} 
