package com.example.jhibbele.handlerthread;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.lang.ref.WeakReference;

public class WorkerBee<T> extends HandlerThread {
    private static final String TAG = "WorkerBee";
    private static final int MESSAGE_ID = 0;
    private Handler mRequestHandler;
    private Handler mResponseHandler;

    public WorkerBee(Handler responseHandler) {
        super(TAG);
        mResponseHandler = responseHandler;
    }

    public void queueMessage(T target) {
        Log.i(TAG, "queueMessage " + (String) target);
        mRequestHandler.obtainMessage(MESSAGE_ID, target).sendToTarget();
    }

    @Override
    protected void onLooperPrepared() {
        mRequestHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == MESSAGE_ID) {
                    T target = (T) msg.obj;
                    Log.i(TAG, "got a request: " + target);
                    handleRequest(target);
                }
            }
        };
    }

    public void clearQueue() {
        mRequestHandler.removeMessages(MESSAGE_ID);
    }

    private void handleRequest(final T target) {
        Log.i(TAG, "process request " + (String) target);
        String s = (String) target + " plus stuff";
        mResponseHandler.obtainMessage(MESSAGE_ID, s).sendToTarget();
    }
}
