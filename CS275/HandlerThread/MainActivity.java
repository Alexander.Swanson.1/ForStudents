package com.example.jhibbele.handlerthread;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "HandlerThread";
    private static int MESSAGE_ID = 0;
    private int requestNumber = 0;

    private Button mRequestButton;
    private TextView mTextView;
    private WorkerBee mWorkerBee;
    private Handler mResponseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRequestButton = (Button) findViewById(R.id.RequestButton);
        mTextView = (TextView) findViewById(R.id.TextView);

        mResponseHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == MESSAGE_ID) {
                    String target = (String) msg.obj;
                    Log.i(TAG, "got a response: " + target);
                    mTextView.setText(target);
                }
            }
        };

        mWorkerBee = new WorkerBee<>(mResponseHandler);
        mWorkerBee.start();
        mWorkerBee.getLooper();

        mRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mStopButton.setEnabled(true);
                ++requestNumber;
                String msg = String.format("message %d", requestNumber);
                mWorkerBee.queueMessage(msg);
            }
        });
    } // onCreate()

    @Override
    public void onDestroy() {
        super.onDestroy();
        mWorkerBee.clearQueue();
        mWorkerBee.quit();
    }
}