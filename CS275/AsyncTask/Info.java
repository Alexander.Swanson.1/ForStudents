package com.example.jhibbele.asynctask;

public class Info {
    private String string;

    public Info() {
        string = "";
    }

    public Info(String s) {
        string = s;
    }

    public String getString() {
        return string;
    }

    public String toString() {
        return string;
    }
}
