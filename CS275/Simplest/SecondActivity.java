package com.example.jhibbele.simplest2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.jhibbele.simplest2.SimplestActivity.MESSAGE;

public class SecondActivity extends AppCompatActivity {
    Button mButton;
    TextView mMessageTextField;

    public static Intent newIntent(Context context, String message) {
        Intent intent = new Intent(context, SecondActivity.class);
        intent.putExtra(MESSAGE, message);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String message = "no magic";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        if (getIntent().hasExtra(MESSAGE)) {
            message = getIntent().getStringExtra(MESSAGE);
        }

        mButton = (Button) findViewById(R.id.second_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "click", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        mMessageTextField = (TextView) findViewById(R.id.message_text);
        mMessageTextField.setText(message);
    }

}