<?php

/**
 * An example of the object you could return from your server. If your API 
 * returns data in the same format on every request things will go quicker. 
 * Putting the response into an object forces this to be the case. This allows
 * you to predictably parse the response client side. So, you can edit/extend 
 * this class however you'd like to fit your needs.
 *
 * authored by Ryan Berliner, 10/2018
 */
class ExampleResponseClass {

  private $success;
  private $message;
  private $data;

  public static $default_error = 'An unknown error has occured.';

  /**
   * Notice how this defaults to failure? That way if something goes wrong 
   * somewhere you didn't account for you won't be told otherwise.
   * @param boolean $success Did you process the request successfully?
   * @param String  $message Do you want to send a message back to the client?
   *                         (ex. For errors you might elaborate here)
   * @param array   $data    The data you came for! (ex. an array of objects)
   */
  public function __construct( $success = false, String $message = null, $data = array() )
  {
    $this->success = $success;
    $this->message = ( $message == null ) ? self::$default_error : $message;
    $this->data = $data;
  }

  /**
   * When its time to send your response back to the client, you should put it 
   * in a predictable, parsable format. That's what this does by creating an 
   * associative array, and then encoding it into a json object. It will echo 
   * it to the client, then die() 
   * 
   * Any code placed after you call this function will not be hit so long as the 
   * die() is there.
   * @return void
   */
  public function send_response()
  {
    $response = array(
      'success' => $this->success,
      'message' => $this->message,
      'data'    => $this->data
    );
    echo json_encode( $response, JSON_FORCE_OBJECT );
    die();
  }

  public function is_success()
  {
    return $this->success;
  }

  public function get_message()
  {
    return $this->message;
  }

  public function get_data()
  {
    return $this->data;
  }

  public function set_success( $success )
  {
    $this->success = $success;
  }

  public function set_message( $message )
  {
    $this->message = $message;
  }

  public function set_data( $data )
  {
    $this->data = $data;
  }

}

